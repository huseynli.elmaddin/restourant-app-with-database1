public class Menu {

    int cost;
    String ProductName;

    int id;
    int productId;


    public Menu (int cost, String productName,int productId){
        this.cost=cost;
        this.ProductName=productName;
        this.productId=productId;

    }

    public Menu (){

    }
    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCost() {
        return cost;
    }

    public String getProductName() {
        return ProductName;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "cost=" + cost +"$ "+
                ", ProductName='" + ProductName + '\'' +
                ", id=" + id +
                '}';
    }
}
